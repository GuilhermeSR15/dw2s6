package br.edu.ifsp.arq.dw2.cervejeiro.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifsp.arq.dw2.cervejeiro.enumeration.Origem;
import br.edu.ifsp.arq.dw2.cervejeiro.enumeration.Sabor;
import br.edu.ifsp.arq.dw2.cervejeiro.model.Cerveja;
import br.edu.ifsp.arq.dw2.cervejeiro.repository.Estilos;
import br.edu.ifsp.arq.dw2.cervejeiro.service.CadastroCervejaService;

@Controller
public class CervejasController {

	@Autowired
	private Estilos estilos;
	@Autowired
	private CadastroCervejaService cadastroCervejaService;
	
	@RequestMapping(value = "/cervejas/novo")
	public ModelAndView novo(Cerveja cerveja) {
		/*Optional<Cerveja> oc = cervejas.findBySku("asd123");
		System.out.println(oc.isPresent());*/
		ModelAndView mv = new ModelAndView("cerveja/cadastro-cerveja");
		mv.addObject("sabores", Sabor.values());
		mv.addObject("estilos", estilos.findAll());
		mv.addObject("origens", Origem.values());
		return mv;
	}

	@RequestMapping(value = "/cervejas/novo", method = RequestMethod.POST)
	public ModelAndView cadastrar(@Valid Cerveja cerveja, BindingResult result, Model model, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return novo(cerveja);
		}
		cadastroCervejaService.salvar(cerveja);
		attributes.addFlashAttribute("mensagem", "Cerveja salva com sucesso");
		return new ModelAndView("redirect:/cervejas/novo");
	}
}
