package br.edu.ifsp.arq.dw2.cervejeiro.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.ifsp.arq.dw2.cervejeiro.model.Cerveja;

@Repository
public interface Cervejas extends JpaRepository<Cerveja, Integer> {
		public Optional<Cerveja> findBySku(String sku);
		public Optional<Cerveja> findBySkuIgnoreCase(String sku);
}
