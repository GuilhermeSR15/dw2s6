package br.edu.ifsp.arq.dw2.cervejeiro.service.exception;

public class NomeEstiloJaCadastradoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NomeEstiloJaCadastradoException(String message) {
		super(message);
	}
}
