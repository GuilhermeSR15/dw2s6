package br.edu.ifsp.arq.dw2.cervejeiro.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import br.edu.ifsp.arq.dw2.cervejeiro.service.CadastroCervejaService;
import br.edu.ifsp.arq.dw2.cervejeiro.storage.FotoStorage;
import br.edu.ifsp.arq.dw2.cervejeiro.storage.local.FotoStorageLocal;

@Configuration
@ComponentScan(basePackageClasses = CadastroCervejaService.class)
public class ServiceConfig {

	@Bean
	public FotoStorage fotoStorage() {
		return new FotoStorageLocal();
	}
	
}
