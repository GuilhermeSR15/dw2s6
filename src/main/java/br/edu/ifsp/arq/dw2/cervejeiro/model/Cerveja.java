package br.edu.ifsp.arq.dw2.cervejeiro.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import br.edu.ifsp.arq.dw2.cervejeiro.enumeration.Origem;
import br.edu.ifsp.arq.dw2.cervejeiro.enumeration.Sabor;

@Entity
@Table(name = "cerveja")
public class Cerveja implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@NotBlank(message = "SKU é obrigatório")
	private String sku;

	@NotBlank(message = "Nome é obrigatório")
	private String nome;

	@NotBlank(message = "Descrição deve não pode estar em branco")
	@Size(min = 3, max = 50, message = "Descrição deve estar entre 3 e 50 caracteres")
	private String descricao;

	@NotNull(message = "Valor deve ser informado")
	@DecimalMin(value = "0.5", message = "Valor deve ser maior que R$0,50")
	@DecimalMax(value = "999999.99", message = "Valor deve ser menor que R$999999,99")
	private BigDecimal valor;

	@Column(name = "quantidade_estoque")
	@NotNull(message = "Estoque não pode ser vazio")
	private Integer quantidadeEstoque;

	@Enumerated(EnumType.STRING)
	@NotNull(message = "Origem deve ser informada")
	private Origem origem;

	@Enumerated(EnumType.STRING)
	@NotNull(message = "Sabor deve ser informado")
	private Sabor sabor;

	@ManyToOne
	@JoinColumn(name = "codigo_estilo")
	@NotNull(message = "Estilo deve ser informado")
	private Estilo estilo;
	
	@Column(name="teor_alcoolico")
	@NotNull(message = "Teor alcoólico não pode estar em branco")
	@DecimalMax(value = "100.00", message = "Valor alcoólico deve ser menor ou igual a 100")
	private BigDecimal teorAlcoolico;
	
	@NotNull(message = "Comissão não pode ser vazia")
	@DecimalMax(value = "100.00", message = "Comissão deve ser menor ou igual a 100")
	private BigDecimal comissao;
	
	private String foto;
	
	@Column(name="content_type")
	private String contentType;
	
	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public BigDecimal getComissao() {
		return comissao;
	}

	public void setComissao(BigDecimal comissao) {
		this.comissao = comissao;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Integer getQuantidadeEstoque() {
		return quantidadeEstoque;
	}

	public void setQuantidadeEstoque(Integer quantidadeEstoque) {
		this.quantidadeEstoque = quantidadeEstoque;
	}

	public Origem getOrigem() {
		return origem;
	}

	public void setOrigem(Origem origem) {
		this.origem = origem;
	}

	public Sabor getSabor() {
		return sabor;
	}

	public void setSabor(Sabor sabor) {
		this.sabor = sabor;
	}

	public Estilo getEstilo() {
		return estilo;
	}

	public void setEstilo(Estilo estilo) {
		this.estilo = estilo;
	}

	public BigDecimal getTeorAlcoolico() {
		return teorAlcoolico;
	}

	public void setTeorAlcoolico(BigDecimal teorAlcoolico) {
		this.teorAlcoolico = teorAlcoolico;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cerveja other = (Cerveja) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
	@PrePersist
	@PreUpdate
	private void prePersistUpdate() {
		this.sku = sku.toUpperCase();
	}
}
