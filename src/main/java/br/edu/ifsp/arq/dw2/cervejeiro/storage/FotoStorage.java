package br.edu.ifsp.arq.dw2.cervejeiro.storage;

import org.springframework.web.multipart.MultipartFile;

public interface FotoStorage {

	public String salvarTemporariamente(MultipartFile[] foto);

	public byte[] recuperarFotoTemporaria(String nome);
}
